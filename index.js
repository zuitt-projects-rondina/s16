function displayMsgToSelf(){

	console.log("You can do it!");
}
/*displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
displayMsgToSelf();
*/

let count = 10;

while(count !== 0){
	displayMsgToSelf()
	count--;

};

//WHILE LOOP 
	//Allow us to repeat an instruction a long as the condition is true;
	/*
		SYNTAX
			while(condition is true){
							
				statement
			}	

	*/

let number = 5;
	//countdow
	while(number !== 0){
		console.log("While" + " " + number);
		number--		
	};
	//count from 0 to 10

	while(number <= 10){
		console.log("While" + " " + number);
		number++;	
	};



// DO-WHILE LOOP
	// works a lot like while loop, but unlike while loop, do-while guarantees to atleast once


	// Number functions change a string type to number data type
	let number2 = Number(prompt("Give me a number"))

	do {

		//the current value of number2 is printed
		console.log(`Do While ${number2}`)
		//increases the number by 1
		number2 += 1;
	} while (number2 < 10);


	let counter = 1;

	do {
		console.log(counter);
		counter++
	} while (counter <= 21);


//FOR LOOP 
		// more flexible loop tjam while and do-while
		//consist of 3 parts ; initialization, condition, finalExpression


	
	for(let count = 0; count <= 20; count++){

		console.log(count)
	};

	let myString = "alex";
	console.log(myString.length);
		//result : 4

	// NOTE = STRINGS ARE SPECIAL COMPARED TO OTHER DATA TYPES
		// IT HAS ACCESS TO FUNCTION AND OTHER PRIMITIVE PIECES OF INFO
		// ANOTHER PRIMITIVE MIGHT NOT HAVE

	console.log(myString[0]);
		//result : a

	for(let x = 0 ; x < myString.length ; x++){

		console.log(myString[x]);
	};


	let myName = "Vanvan";

	for(i = 0; i < myName.length; i++){

		if(
			myName[i].toLowerCase() == "a"	||
			myName[i].toLowerCase() == "e"	||
			myName[i].toLowerCase() == "i"	||
			myName[i].toLowerCase() == "o"	||
			myName[i].toLowerCase() == "u"	
			){
				console.log(3);

		} else {
				console.log(myName[i]);
		}
	};

	// CONTINUE AND BREAK STATEMENTS

	for(let count = 0 ; count <= 20 ; count ++){

		if(count % 2 === 0){
			continue;
		}

		console.log(`Contine and Break ${count}`)

		if(count > 10) {
			break;
		}
	};


	let name = "alexandro";

	for ( let i = 0 ; i < name.length; i ++){

		console.log(name[i]);

		if(name[i].toLowerCase() === "a"){

			console.log("Continue to next iteration")
			continue;
		}

		if(name[i] == "d"){
			break;
		}

	};
